import React from 'react';

//Creates a Context Object
// Context object - data type of an object that can be store information that can be shared to other component within the app
const UserContext = React.createContext();

// Provider components allows other components to consume/use the context object and supply the necessary information needed
export const UserProvider = UserContext.Provider;

export default UserContext;