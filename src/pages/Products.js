import {useState, useEffect, useContext} from 'react';
import {Row} from 'react-bootstrap';
import productData from '../data/productData';
import ProductCard from '../components/ProductCard';
import AdminProduct from '../components/AdminProduct';
//import AdminAddProduct from '../components/AdminAddProduct';
import UserContext from '../UserContext';
import '../App.css';

export default function Products(){

	const {user} = useContext(UserContext);
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://capstone2-perezeena-eena.onrender.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			const productsArr = (data.map(product => {
				return(
					<ProductCard key={product._id} productProp = {product} breakpoint={3} />
				)
			}))
			setProducts(productsArr)
		});

		

	},[]);


	return (
		//<ProductCard/>
		(user.isAdmin !== true) ?
		<>
		<Row>
		<h1 className="product-header"> Available Courses: </h1>
		{products}
		</Row>
		</>
		:
		<>
		
		<AdminProduct/>
		</>
	)
}