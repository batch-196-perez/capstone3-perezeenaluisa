import {Row, Col, Button, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function errorPage(){
	return (
		<Row>
			<Col className = "error p-5">
			<h1>404 - Not Found</h1>
				<p>The page you are looking for cannot be found</p>
				<Button variant="primary" as={Link} to="/">Back Home</Button>
			</Col>
		</Row>
	)
} 