import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import AllOrderCard from '../components/AllOrderCard';
import UserContext from '../UserContext';


export default function UserOrder() {
    const {user, setUser} = useContext(UserContext);
    const redirect = useNavigate();
    const [orders, setOrders] = useState([]); 
    
    useEffect(() => {
        fetch("https://capstone2-perezeena-eena.onrender.com/users/getAllOrder", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then((data) => {
            // console.log(data);

            setOrders(data.map(orderList => {
                return (
                    <AllOrderCard key={orderList._id} orderProp={orderList} />
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin) ?
        <>
            <Table bordered className="table mt-5">
                <thead>
                    <tr className="table-header">
                        <th className="table-header-content">Customer Name</th>
                        <th className="table-header-content">Product Name</th>
                        <th className="table-header-content">Quantity</th>
                        <th className="table-header-content">Total Amount</th>
                       	<th className="table-header-content">Date Purchased</th>
                       	<th className="table-header-content">Order Id</th>
                    </tr>
                </thead>
                <tbody>
                    { orders }
                </tbody>
            </Table>
            
        </>
        :
        <Navigate to="/products"/>
    );
}
