

import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Form, Button, Container, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register(){

	const{user} = useContext(UserContext);
	const history = useNavigate();

	const[firstName,setFirstName] = useState('');
	const[lastName,setLastName] = useState('');
	const[mobileNo,setMobileNo] = useState('');
	const[email,setEmail] = useState('');
	const[password,setPassword] = useState('');


	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('https://capstone2-perezeena-eena.onrender.com/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				Swal.fire({
		 			title: "Duplicate email found",
		 			icon: "info",
		 			text: "The email that you're trying to register already exist"
		 		});
			} else {

				fetch('https://capstone2-perezeena-eena.onrender.com/users', {
				 			method: 'POST',
				 			headers: {
				 				'Content-Type' : 'application/json'
				 			},
				 			body: JSON.stringify({
				 				firstName: firstName,
				 				lastName: lastName,
				 				email: email,
				 				mobileNo: mobileNo,
				 				password: password
				 			})
				 	})
				 		.then(res => res.json())
				 		.then(data => {
				 			console.log(data);

				 			if(data.email){
				 				Swal.fire({
				 					title: 'Registration successful!',
				 					icon: 'success',
				 					text: 'Thank you for registering'
				 				});
				 				
				 				history("/login");

				 			} else {
				 				Swal.fire({
				 					title: 'Registration failed',
				 					icon: 'error',
				 					text: 'Something went wrong, try again'
				 				});
				 			};
				 		});
			}
		})
		setEmail('');
		setPassword('');
		setFirstName('');
		setLastName('');
		setMobileNo('');

	};
		

		 


		//alert('Thank you for registering');
	

	useEffect(() => {
		//validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && password !== '' && firstName !=='' && lastName !== '' && mobileNo !=='' && mobileNo.length === 11 ) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password, firstName, lastName, mobileNo]);

	return (
		(user.id !== null ) ?
		<Navigate to="/products"/>
		:
		<>
		<Row className="rowRegister">
			<Col className="colRegister"><p className="register-banner">Register to buy and enjoy the gifts for your furbabies</p>
			</Col>
			<Col className="">
				<Container className="register">
					<h1 className="registerHeader">Register Here: </h1>
					<Form onSubmit={e => registerUser(e)}>
						
						<Form.Group controlId="firstName" className="registerFormTitle">
							<Form.Label>First Name</Form.Label>
							<Form.Control
								className="registerInput"
								type="text"
								placeholder="Enter your first name here"
								required
								value = {firstName}
								onChange = {e => setFirstName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="lastName" className="registerFormTitle">
							<Form.Label>Last Name</Form.Label>
							<Form.Control
								className="registerInput"
								type="text"
								placeholder="Enter your last name here"
								required
								value = {lastName}
								onChange = {e => setLastName(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="mobileNo" className="registerFormTitle">
							<Form.Label>Mobile No</Form.Label>
							<Form.Control
								className="registerInput"
								type="text"
								placeholder="Enter your mobile number here"
								required
								value = {mobileNo}
								onChange = {e => setMobileNo(e.target.value)}
							/>
						</Form.Group>

						<Form.Group controlId="userEmail" className="registerFormTitle">
							<Form.Label>Email Address</Form.Label>
							<Form.Control
							 	className="registerInput"
								type="email"
								placeholder="Enter your email here"
								required
								value = {email}
								onChange = {e => setEmail(e.target.value)}
							/>
							<Form.Text className="text-muted">
								We'll never share your email with anyone else.
							</Form.Text>	
						</Form.Group>

						<Form.Group controlId="password">
							<Form.Label>Password</Form.Label>
							<Form.Control
								className="registerInput"
								type="password"
								placeholder="Enter your password here"
								required
								value = {password}
								onChange = {e => setPassword(e.target.value)}
							/>
						</Form.Group>
						
						<p>Already registered? <Link to="/login">Sign in here</Link></p>
						{ isActive ? 
							<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register
							</Button>

							:

							<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Register
							</Button>

						}
					</Form>
						
				</Container>
			</Col>
			
		</Row>
		
		</>


	)
}

/*
<Form className="form">
				<Form.Group className="form-input" controlId="firstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						className = "forminput"
						type="text"
						placeholder="Enter your first name here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="lastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your last name here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter your 11-digit mobile number here"
						required
						value = ""
						
					/>
				</Form.Group>

				<Form.Group className="form-input" controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter your email here"
						required
						value = ""
						
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>	
				</Form.Group>

				<Form.Group className="form-input" controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your password here"
						required
						value = ""
						
					/>
				</Form.Group>
					<Button className="buttonko mt-3 mb-5" variant="success" type="submit" id="submitBtn">Register
					</Button>

			</Form>
			*/