import {Container, Row, Col, Card, Table} from 'react-bootstrap';
import {useState, useEffect, useContext, Div} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import  UserContext from '../UserContext';
import {Link} from 'react-router-dom';
import UserOrder from '../components/UserOrder';
import '../App.css';

import {FaUserCircle} from 'react-icons/fa';

export default function Profile(){

	const {user} = useContext(UserContext);
	// console.log(user);

	const [profile, setProfile] = useState([]);

	const[firstName, setFirstName] = useState('');
	const[lastName, setLastName] = useState('');
	const[mobileNo, setMobileNo] = useState('');
	const[email, setEmail] = useState('');
	//const[products, setProduct] = useState('');
	const[orders, setOrders] = useState('');

	useEffect(() => {
		fetch('https://capstone2-perezeena-eena.onrender.com/users/userDetails',{
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setMobileNo(data.mobileNo);
			setEmail(data.email);
			// setOrders(data.orders);
			//setProduct(data.orders);
			
		});
	})


	return (

		
			(user.id !== null) ?
			<>
					<div className="profile mt-5">
						<div className="pro">
						<Row className="profile-row">
							<Col lg={3} className="profile-section">
								<h5 className="profile-header">User's Profile</h5>
								<div className="profile-logo">
									<FaUserCircle className="profile-icon"/>
								</div>
								<p className="profile-header"><b> {firstName}  {lastName}</b></p>
								<p className="text-center"><b>Details</b></p>
								<p className="">Mobile No: {mobileNo}</p>
								<p className="">Email: {email}</p>
							</Col>
							<Col lg={9} className="order-section">
							<UserOrder/>
							</Col>
							</Row>		
						</div>

					</div>
			</>
			:
			<Navigate to="/products"/>
			
		)



}