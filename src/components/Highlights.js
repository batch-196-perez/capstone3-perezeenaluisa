import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Dress For My Babies</h2>
						</Card.Title>
						<Card.Text>
							Dress the babies like their hoooomans!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Spoiling Toys</h2>
						</Card.Title>
						<Card.Text>
							Let your babies enjoy their stay here through playing. Our babies loves to play so spoil them with love. 
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Give A Treat</h2>
						</Card.Title>
						<Card.Text>
							Treat your buddies by giving them treats. It also helps you to teach them tricks and bonds you together.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}