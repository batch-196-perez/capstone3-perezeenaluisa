
import {Row, Col, Button} from 'react-bootstrap';
import '../App.css';
import {Link} from 'react-router-dom';

export default function Banner(){
	return (
		<Row>
			<Col className ="banner p-5">
					<h1 className="banner-text">Treat Your Buddy</h1>
					<p className="banner-text">Treat your bestfriend by giving them their needs.</p>
					<Link as={Link} to="/products"className="banner-btn">Explore Products</Link>
			</Col>
		</Row>

	)
}