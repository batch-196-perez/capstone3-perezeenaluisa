
import {useState, useEffect,useContext} from 'react';
import {Table, Button, Modal, Form} from 'react-bootstrap';
import AdminProductCard from '../components/AdminProductCard';
// import {useParams, useNavigate, Link} from 'react-router-dom';
//import AdminAddProduct from '../components/AdminAddProduct';
import Products from '../pages/Products';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import {MdAddBox} from 'react-icons/md'
import '../App.css';

export default function AdminProductView(){

	const {user} = useContext(UserContext);

	// const {breakpoint, productProp} = props
	//const {name, description, price, _id} = productProp;

	const [products, setProducts] = useState([]);
	
	//const history = useNavigate();

	// useStates for Add
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActiveBtn, setIsActiveBtn] = useState(false);

	const [showAdd, setShowAdd] = useState(false)
	const handleAddClose = () => setShowAdd(false);
	const handleAddShow = () => setShowAdd(true);

	const [addedProduct, setAddedProduct] = useState(false);
	const [dis, setDis] = useState(()=> setProducts)

	//Function for adding products
	function addNewProduct(){
		 // e.preventDefault();

		fetch('https://capstone2-perezeena-eena.onrender.com/products/addProduct',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
		 				name: name,
		 				description: description,
		 				price: price
			 	})
		 	})	
		 	.then(res => res.json())
		 	.then(data => {
		 		console.log(data)
		 			Swal.fire({
		 				title: 'New Product Added Successfully!',
		 				icon: 'success',
		 				text: 'Add more products'
		 				});
		 	// setAddedProduct(true);
			setShowAdd(false);
			setName('');
			setDescription('');
			setPrice('');

		 	})
		}
	
	useEffect(() => {
		// validation to enable the submit button when all fields are populated and both passwords match
		if(name !== '' && description !== '' && price!== '' && price !== 0){
			setIsActiveBtn(true);

		} else {
			setIsActiveBtn(false);
		}

	}, [name, description, price]);


	useEffect(()=> {
		fetch('https://capstone2-perezeena-eena.onrender.com/products/getAllProducts',{
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			const productsArr = (data.map(product => {
				return(
					<AdminProductCard key={product._id} productProp = {product}/>
				)
			}))
			setProducts(productsArr)
		});
	},[name, description, price]);

	return (
		(user.id === null)?	
		<Products/>
		:
		<>
		<h1>Admin</h1>
		<Table striped hover className="table">
      		<thead>
      		<tr >
      		<th colSpan={6} className="text-center mb-0"><h3 className="mb-0">PRODUCT TABLE</h3></th>
      		<th colSpan={2}  className="text-center"><Button className="productAdminAdd" onClick={handleAddShow}><MdAddBox/>  Add Product</Button></th>
      		</tr>
      		<tr  className="table-header" >
			       <th className="table-header-content">NAME</th>
			       <th className="table-header-content">DESCRIPTION</th>
			       <th className="table-header-content">PRICE</th>
			       <th className="table-header-content">STATUS</th>
			       <th className="table-header-content">DELETE</th>
			       <th className="table-header-content">UPDATE</th>
			       <th className="table-header-content">DISABLE</th>
		        </tr>
      		</thead>
      		{/*{ 
      			addedProduct !== true ?
      			<tbody>	
      				{products}
      			</tbody>*/}
      		{/*	:
      			<>*/}
      			<tbody>
      				{products}
      			</tbody>
      		{/*	</>
      		}*/}
    	</Table>
    	

      <Modal show={showAdd} onHide={handleAddClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={e => addNewProduct()}>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Here"
                value={name}
                autoFocus
                onChange={e => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Product Description</Form.Label>
              <Form.Control 
              as="textarea" 
              rows={3} 
              placeholder="Product Description Here"
              value={description}
              onChange={e => setDescription(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Price"
                value={price}
                onChange={e => setPrice(e.target.value)}
                autoFocus
              />
            </Form.Group>


          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleAddClose}>
            Close
          </Button>
          { isActiveBtn ?
            <Button variant="primary" type="submit" id="submitBtn" onClick={(e) => addNewProduct()}>
              Save New Product
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
              Save New Product
            </Button>
          }

        </Modal.Footer>
      </Modal>
		</>
	)
}



// https://blooming-beyond-78703.herokuapp.com