import { useEffect, useState, useContext } from "react";

export default function AllOrderCard({ orderProp }) {

    const { firstName, lastName } = orderProp;


 	useEffect(()=>{
 	})
    return (

        orderProp.orders.map((orderList) =>
            orderList.products.map((productList) =>
               
                <tr key={productList._id}>
                    <td>{firstName} {lastName}</td>              
                    <td>{ productList.productName }</td>
                    <td>{ productList.quantity }</td>
                    <td>{ orderList.totalAmount }</td>
                    <td>{ orderList.purchasedOn }</td>
                    <td>{ orderList._id }</td>
                </tr>
                
            )
        )
    );
}