


import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button, ButtonGroup} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {RiShoppingCart2Line} from 'react-icons/ri';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName]= useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [totalAmount, setTotalAmount] = useState("");
	const [quantity, setQuantity] = useState(1);

	const {productId} = useParams();

	const buy = (productId) => {
		if(quantity === 0 ){
			Swal.fire({
					title: 'Add quantity',
					icon: 'error',
					text: 'Make an order'
				});
		} else {
			fetch('https://capstone2-perezeena-eena.onrender.com/users/addOrder',{
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					totalAmount : price * quantity,
					products: [
						{
							productId: productId,
							quantity: quantity,
							productName:name,
							price: price
						}
					]
				})
			})
			.then(res => res.json())
			.then(data => {
				if(data){
					Swal.fire({
						title: 'Successfully ordered!',
						icon: 'success',
						text: 'Thank you for ordering'
					});

					history("/products");

				} else {
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again later'
					});
				};

			});
		}

		
	};

	function addQuantity(){
		// if (quantity <= 0) {
		// 		setQuantity(quantity)
		// } else {
			setQuantity(quantity + 1 )
		// }

	}

	function deductQuantity(){
	
		if (quantity <= 0) {
				setQuantity(quantity)
		} else {
			setQuantity(quantity -1 )
		}

	}

	useEffect(() => {
		console.log(productId)
		fetch(`https://capstone2-perezeena-eena.onrender.com/products/singleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
		});

	}, [productId]);

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{span:4, offset:4}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<ButtonGroup size="sm">
							    <Button className="buttonFunction" onClick={() => deductQuantity()}> - </Button>
							    <Button variant="light" disabled>{quantity}</Button>
							    <Button className="buttonFunction" onClick={() => addQuantity()}> + </Button>
							 </ButtonGroup>

							{ user.id !== null ?
								<Button  className="buyButton" size="sm" onClick={() => buy(productId)}> <RiShoppingCart2Line className="me	-2"/> Buy</Button>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}