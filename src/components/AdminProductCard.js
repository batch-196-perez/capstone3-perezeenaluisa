import {useState,useEffect, useContext} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
// import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {AiFillDelete} from 'react-icons/ai';
import {FaEdit} from 'react-icons/fa';
import {BsFillEyeSlashFill, BsFillEyeFill} from 'react-icons/bs';
import '../App.css'

export default function ProductCard({productProp}){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const {name, description, price, _id, isActive} = productProp;
	//console.log(productProp._id);

	

	// for Activate
	const [activeProduct, setActiveProduct] = useState(true)

	// for delete
	const [showDelete, setShowDelete] = useState()
	const handleCloseDel = () => setShowDelete(false);
	const handleShowDel = () => setShowDelete(true);

	// const id = productProp.id;

	const [productEditName, setProductEditName] = useState(name);
	const [productEditDescription, setProductEditDescription] = useState(description);
	const [productEditPrice, setProductEditPrice] = useState(price);


	const [productName, setProductName] = useState(productEditName);
	const [productDescription, setProductDescription] = useState(productEditDescription);
	const [productPrice, setProductPrice] = useState(productEditPrice);
	const [productIsActive, setProductisActive] = useState(isActive);

	// for Edit 
	const [showEdit, setShowEdit] = useState(false);
	const handleCloseEdit = () => {
		setShowEdit(false);
		// setProductEditName(productName);
		// setProductEditDescription(productDescription);
		// setProductEditPrice(productPrice);
	}
	const handleShowEdit = () => setShowEdit(true);	
	const [deletedProduct, setDeletedProduct] = useState(false);

	let status = "On Sale";

	if (isActive){
		status ='On Sale';
	} else {
		status ='Not On Sale';
	}

	const [productStatus, setProductStatus] = useState(status);



	function pageReload() {
        window.location.reload(true);
  }

	function updateProduct(_id) {	
		fetch(`https://capstone2-perezeena-eena.onrender.com/products/updateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productEditName,
				description: productEditDescription,
				price: productEditPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
			setProductEditName(data.name);
			setProductEditDescription(data.description);
			setProductEditPrice(data.price);
			
			setProductName(productEditName);
			setProductDescription(productEditDescription);
			setProductPrice(productEditPrice);

			setShowEdit(false)
		})
	}

	//Activating

	function activateProduct(_id){

		fetch(`https://capstone2-perezeena-eena.onrender.com/products/activateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
			
		})
			setProductisActive(true);
			setProductStatus("On Sale")
	}

	//Deactivate
	function deactivateProduct(_id){

		fetch(`https://capstone2-perezeena-eena.onrender.com/products/archiveProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
				setProductisActive(false);
				setProductStatus("Not On Sale")
		})
	}

	function deleteProduct(_id){
			fetch(`https://capstone2-perezeena-eena.onrender.com/products/deleteProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			// Swal.fire({
		 // 			title: 'Deleted Product',
		 // 			icon: 'success',
		 // 			text: 'The product is already updated'
		 // 		});
					setShowDelete(false);
					setDeletedProduct(true);
		})
		}

	useEffect(() => {
		
		
	},[])


	return (
		(deletedProduct !== true) ? 
		<>
		<tr className="" bordered>
          <td>{productName}</td>
          <td>{productDescription}</td>
          <td className="text-end">{productPrice}</td>
          <td className="text-center">{productStatus}</td>
          <td className="text-center"><Button className="productAdminButton btn-delete" onClick={() => deleteProduct(_id)}><AiFillDelete/> Delete</Button></td>
          <td className="text-center"><Button className="productAdminButton btn-edit" onClick={handleShowEdit}><FaEdit/>  Update</Button></td>
          <td className="text-center">
          { productIsActive === true ? 
             <Button className="productAdminButton btn-deact"onClick={e => deactivateProduct(_id)}><BsFillEyeSlashFill/> Deactivate</Button>
            :
             <Button className="productAdminButton btn-act" onClick={e => activateProduct(_id)}><BsFillEyeFill/> Activate</Button>
          }
          </td>
        </tr>

		<Modal show={showEdit} onHide={handleCloseEdit} >
			        <Modal.Header closeButton>
			        <Modal.Title>Update Product's Info</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form >
							<Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Name</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditName}
						        onChange = {e => setProductEditName(e.target.value)}
						        autoFocus
						    />
							</Form.Group>

						    <Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Description</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditDescription}
						        onChange = {e => setProductEditDescription(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="productPrice">
						    <Form.Label>Price</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productEditPrice}
						        onChange = {e => setProductEditPrice(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						</Form>
						</Modal.Body>
					<Modal.Footer>
					   
					    { (productEditName === productName && productEditDescription === productDescription && productEditPrice === productPrice ) ?
					    	<Button variant="danger" onClick={() => updateProduct(_id)} disabled>
					    					    Save Changes
					    	</Button>
					    					    :
					    	<Button variant="primary" onClick={() => updateProduct(_id)}>
					    					Save Changes
					    	</Button>
					    					  }
					</Modal.Footer>
			</Modal> 


			<Modal show={showDelete} onHide={handleCloseDel}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you will delete this product?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDel}>
            Close
          </Button>
          <Button variant="primary" onClick={() => deleteProduct(_id)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
			</>
			:
		 deleteProduct(_id)
	)
}