import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import UserOrderCard from '../components/UserOrderCard';
import UserContext from '../UserContext';
import '../App.css';

export default function UserOrder() {
    const {user, setUser} = useContext(UserContext);
    const redirect = useNavigate();
    const [userOrders, setUserOrders] = useState([]); 
    
    useEffect(() => {
        fetch("https://capstone2-perezeena-eena.onrender.com/users/getUserOrder", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then((data) => {
           console.log(data);

            setUserOrders(data.map(orderData => {
                return (
                    <UserOrderCard key={orderData._id} userOrderProp={orderData} />
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin === false) ?
        <>
            <Table bordered className="table mt-5">
                <thead>
                    <tr className="table-header">
                        <th className="table-header-content">Order ID</th>
                        <th className="table-header-content">Product</th>
                        <th className="table-header-content">Quantity</th>
                        <th className="table-header-content">Total Amount</th>
                        <th className="table-header-content">Date Purchased</th>
                    </tr>
                </thead>
                <tbody>
                    { userOrders }
                </tbody>
            </Table> 
        </>
        :
        <>
        <h1>On Progress</h1>
        </>
      
    );
}
