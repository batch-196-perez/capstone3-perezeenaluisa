
import {useState, useContext} from 'react';
import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';
import { RiShoppingBasket2Line } from 'react-icons/ri'
import { RiShoppingCart2Line } from 'react-icons/ri';
import '../App.css';

export default function ProductCard(props){
	const {user} = useContext(UserContext);

	const {breakpoint, productProp} = props

	const {name, description, price, _id} = productProp;

	return (
		<Col xs={12} md={breakpoint} className="mt-4">
		<Card className ="card1 p-3 mb-3">
			<Card.Body>
				<div className="icon-box">
					<Card.Title className="fw-bold "><RiShoppingBasket2Line className="icon"/></Card.Title>
				</div>
				<Card.Title className="fw-bold">{name}</Card.Title>
				<Card.Subtitle>Description: </Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Link className="buttonView" to={`/productView/${_id}`}><RiShoppingCart2Line/>  View Details</Link>
				
			</Card.Body>
		</Card>
		</Col>
	)
}