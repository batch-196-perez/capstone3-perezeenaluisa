

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
//import AdminAddProduct from './components/AdminAddProduct';
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Profile from './pages/Profile';
import Register from './pages/Register';
import {UserProvider} from './UserContext';
import './App.css';



function App() {

  const[user, setUser]= useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch('https://capstone2-perezeena-eena.onrender.com/users/userDetails', {
      headers : {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      };
    });
  },[]);


  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route exact path="/" element={<Home/>}/>
            <Route exact path="/products" element={<Products/>}/>
            <Route exact path="/productView/:productId" element={<ProductView/>}/>
            <Route exact path="/register" element={<Register/>}/>
            <Route exact path="/login" element={<Login/>}/>
            <Route exact path="/adminDashboard" element={<AdminDashboard/>}/>
            <Route exact path="/profile" element={<Profile/>}/>
            <Route exact path="/logout" element={<Logout/>}/>
            <Route exact path="*" element={<Error/>}/>
          </Routes>     
        </Container>
      </Router>
    </UserProvider> 
    </>
  );
}

export default App;
